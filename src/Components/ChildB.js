import React,{useContext} from 'react'  
import ChildC from './ChildC'  
import {UserContext,CategoryContext} from '../App'  
  
function ChildB() {  
    const user = useContext(UserContext)  
    const category = useContext(CategoryContext)  
    return (  
        <div>  
            <ChildC/>  
            Child Component at level 2  
            <div><i>User is {user}, Category is {category}</i></div>  
              
        </div>  
    )  
}  
  
export default ChildB 