import React from 'react'  
import {UserContext,CategoryContext} from '../App'  
  
function ChildC() {  
    return (  
        <div>  
            Child Component at level 3  
            <UserContext.Consumer>  
                {  
                    user => {  
                        return(  
                            <CategoryContext.Consumer>  
                                {  
                                    category=>{  
                                        return <div><b>Context value is {user} and Category value is {category}</b></div>  
                                    }  
                                }  
                            </CategoryContext.Consumer>  
  
                        )   
                    }  
                }  
            </UserContext.Consumer>  
        </div>  
    )  
}  
  
export default ChildC   