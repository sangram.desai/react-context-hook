import React from 'react'  
import ChildA from './ChildA'  
  
function ParentComponent() {  
    return (  
        <div>  
            <ChildA/>              
        </div>  
    )  
}  
  
export default ParentComponent 