import React from 'react'
import ChildB from './ChildB'

function ChildA() {
    return (
        <div>
            <ChildB />
            Child Component at level 1
        </div>
    )
}

export default ChildA