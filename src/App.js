import React from 'react';
import './App.css';
import ParentComponent from './Components/ParentComponent';

export const UserContext = React.createContext()
export const CategoryContext = React.createContext()

function App() {
  return (
    <div className="App">
      <UserContext.Provider value={'ReactApp'} >
        <CategoryContext.Provider value={'Programming'}>
          <ParentComponent />
        </CategoryContext.Provider>
      </UserContext.Provider>
    </div>
  );
}

export default App; 